<!DOCTYPE html>
<html>

<head>
    <title>Tasks Management - Imam Tantowi - 081280901038</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
</head>

<body class="bg-info">
    <div class="container-lg m-5">
        <div class="card">
            <div class="row m-3">
                <div class="col-sm-8">
                    <h2>Tasks <b>Management</b></h2>
                </div>
                <div class="col-sm-4 text-right">
                    <button type="button" class="btn btn-success add-new" data-toggle="modal" data-target="#exampleModal" onclick="task(`Add`)"><i class="fa fa-plus"></i> Add
                        New</button>
                </div>
            </div>
            <div class="row m-3">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input class="form-control" id="title" placeholder="Filter Title">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="description">Description</label>
                        <input class="form-control" id="description" placeholder="Filter Description">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" id="status">
                            <option>-- Choose All --</option>
                            <option value="{{ App\Models\TaskModel::STATUS_PENDING }}">{{ App\Models\TaskModel::STATUS_PENDING }}</option>
                            <option value="{{ App\Models\TaskModel::STATUS_IN_PROGRESS }}">{{ App\Models\TaskModel::STATUS_IN_PROGRESS }}</option>
                            <option value="{{ App\Models\TaskModel::STATUS_COMPLETED }}">{{ App\Models\TaskModel::STATUS_COMPLETED }}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row m-4">
                <div class="table">
                    <table id="datatable" class="table table-bordered" width="100%">
                        <thead class="bg-secondary text-white">
                            <tr>
                                <th width="3%">No</th>
                                <th width="17%">Title</th>
                                <th width="30%">Description</th>
                                <th width="10%">Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
    $(function() {
        var table = $('#datatable').DataTable({
            dom: 'tip',
            processing: true,
            serverSide: true,
            ajax: {
                    url: "{{ route('tasks.list') }}",
                    data: function(d) {
                        d.title = $('#title').val(),
                        d.description = $('#description').val(),
                        d.status = $('#status').val()
                    }
                },
            columns: [{
                    data: "id",
                    sortable: false,
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                }, {
                    data: 'title',
                    name: 'Title'
                }, {
                    data: 'description',
                    name: 'description'
                }, {
                    data: 'status',
                    name: 'status'
                }, {
                    data: 'action',
                    name: 'action',
                    sortable: false
                }]
            });

        $('#title').change(function() {
            table.draw();
        });

        $('#description').change(function() {
            table.draw();
        });

        $('#status').change(function() {
            table.draw();
        });
    });

    function task(button, id, title, description, status) {
        var btn = ''
        var pending_selected = ''
        var in_progress_selected = ''
        var completed_selected = ''
        var disabled = ''
        var title = title ?? ''
        var description = description ?? ''

        if (button != 'View') {
            // Button Add
            btn = `<button type="button" class="btn btn-primary" onclick="store('`+button+`')">`+button+`</button>`
        }else{
            // Views Disabled
            disabled = `disabled`
        }

        // Update Selected
        if (button != 'Add') {
            if (status == "{{ App\Models\TaskModel::STATUS_PENDING }}") pending_selected = "selected"
            if (status == "{{ App\Models\TaskModel::STATUS_IN_PROGRESS }}") in_progress_selected = "selected"
            if (status == "{{ App\Models\TaskModel::STATUS_COMPLETED }}") completed_selected = "selected"
        }

        // Show Modal
        $('#exampleModal').html(`
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">`+button+` Tasks</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body" id="views">
                    <div class="form-group" id="error_title">
                        <label for="title">Title</label>
                        <input class="form-control" id="modal_id" value='`+id+`' hidden>
                        <input class="form-control" id="modal_title" value='`+title+`' placeholder="Input Title" required `+disabled+`>
                    </div>
                    <div class="form-group" id="error_description">
                        <label for="description">Description</label>
                        <textarea class="form-control" id="modal_description" placeholder="Input Description" `+disabled+` rows="5" required>`+description+`</textarea>
                    </div>
                    <div class="form-group" id="modal_view" hidden>
                        <label for="status">Status</label>
                        <select class="form-control" id="modal_status" `+disabled+` required>
                            <option value="{{ App\Models\TaskModel::STATUS_PENDING }}" `+pending_selected+`>{{ App\Models\TaskModel::STATUS_PENDING }}</option>
                            <option value="{{ App\Models\TaskModel::STATUS_IN_PROGRESS }}" `+in_progress_selected+`>{{ App\Models\TaskModel::STATUS_IN_PROGRESS }}</option>
                            <option value="{{ App\Models\TaskModel::STATUS_COMPLETED }}" `+completed_selected+`>{{ App\Models\TaskModel::STATUS_COMPLETED }}</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    `+btn+`
                </div>
            </div>
        </div>
        `)

        // View Show Status
        if (button != 'Add') $('#modal_view').attr('hidden',false)

    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function store(button) {
        fd = new FormData()
        fd.append('title', $('#modal_title').val())
        fd.append('description', $('#modal_description').val())
        fd.append('status', $('#modal_status').val())
        fd.append('id', $('#modal_id').val())

        $.ajax({
            url: "{{ route('tasks.store') }}",
            type: 'post',
            data: fd,
            processData: false,
            contentType: false,
            success: function(result) {
                if (result['status'] == 200){
                    $('#datatable').DataTable().ajax.reload();
                    $('#exampleModal').modal('hide');
                    Swal.fire(
                        button,
                        'Taks '+result['data']['title'],
                        'success'
                    )
                }else{
                    $('.remove_error').remove()
                    if (result['message']['title'] == "The title field is required.") {
                        $('#error_title').append(`<span class="text-danger remove_error" style="font-size:12px">`+result['message']['title']+`</span>`);
                        Swal.fire(
                            'Error '+button,
                            'Error '+ result['message']['title'],
                            'error'
                        )
                    }
                    if (result['message']['title'] == "The title has already been taken.") {
                        $('#error_title').append(`<span class="text-danger remove_error" style="font-size:12px">`+result['message']['title']+`</span>`);
                        Swal.fire(
                            'Error '+button,
                            'Error '+ result['message']['title'],
                            'error'
                        )
                    }
                    if (result['message']['description'] == "The description field is required.") {
                        $('#error_description').append(`<span class="text-danger remove_error" style="font-size:12px">`+result['message']['description']+`</span>`);
                        Swal.fire(
                            'Error '+button,
                            'Error '+ result['message']['description'],
                            'error'
                        )
                    }
                }
            }
        });
    }

    function del(id, title) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this! "+title,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "delete",
                        url: "{{ url('tasks') }}" + '/' + id,
                        data: {
                            "_method": 'delete',
                            "_token": "{{ csrf_token() }}"
                        },
                        success: function(result) {
                            $('#datatable').DataTable().ajax.reload();
                            $('#exampleModal').modal('hide');
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            )
                        }
                    });
                }
            })    }
</script>

</html>
