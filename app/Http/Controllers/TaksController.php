<?php

namespace App\Http\Controllers;

use App\Models\TaskModel;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class TaksController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('tasks.index');
    }

    public function list(Request $request){
        return DataTables::of(
                TaskModel::when($request->title, function($q) use($request){
                    $q->where('title','like','%'.$request->title.'%');
                })
                ->when($request->description, function($q) use($request){
                    $q->where('description','like','%'.$request->description.'%');
                })
                ->when($request->status != "-- Choose All --", function($q) use($request){
                    $q->whereStatus($request->status);
                })
                ->when($request->order, function($q) use($request){
                    switch ($request->order[0]['column']) {
                        case '1': $order = 'title'; break;
                        case '2': $order = 'description'; break;
                        case '3': $order = 'status'; break;
                        default: $order = 'title'; break;
                    }
                    return $q->orderBy($order ,$request->order[0]['dir']);
                })
                ->get()
            )
            ->addColumn('action', function($row){
                return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="task(`View`,`'.$row->id.'`,`'.$row->title.'`,`'.$row->description.'`,`'.$row->status.'`)">View</button>
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal" onclick="task(`Update`,`'.$row->id.'`,`'.$row->title.'`,`'.$row->description.'`,`'.$row->status.'`)">Edit</button>
                <button type="button" class="btn btn-danger" onclick="del('.$row->id.',`'.$row->title.'`)">Delete</button>
                ';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'title'       => "required|unique:tasks,title, $request->id",
            'description' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'status'  => Response::HTTP_BAD_REQUEST,
                'message' => $validator->messages()
            ]);

        $save = TaskModel::firstOrNew([
            'id'  => $request->id
        ]);
        $save->title       = $request->title;
        $save->description = $request->description;
        $save->status      = $request->status ?? 'Pending';
        $save->save();

        return response()->json([
            'status' => Response::HTTP_OK,
            'data'   => $save
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        return response()->json([
            'status'    => Response::HTTP_OK,
            'message'   => TaskModel::findOrFail($id)->delete()
        ]);

    }
}
