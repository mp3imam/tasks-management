<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaskModel extends Model
{
    use HasFactory;
    protected $table = "tasks";
    protected $guarded = ['id'];

    const STATUS_PENDING = "Pending";
    const STATUS_IN_PROGRESS = "In Progress";
    const STATUS_COMPLETED = "Completed";
}
